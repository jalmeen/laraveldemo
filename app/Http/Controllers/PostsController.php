<?php


namespace App\Http\Controllers;
use App\Post;
use DB;

class PostsController extends Controller
{

    public function show($slug){

        //from Database
//        $post = \DB::table('posts')->where('slug',$slug)->first();
        //        dd($post);

        $post = Post::where('slug',$slug)->firstOrFail();

//        if(!$post){
//            abort(404);
//        }


        //handling what is not given in DB

        //get values from defined array
//        $posts = [
//            'my-first-post' => 'Hello, this is my first blog post',
//            'my-second-post' => 'Blog 2 post'
//        ];

//        if(! array_key_exists($post,$posts)){
//            abort(404,'Sorry, not found');
//        }

//        return view('post',[
//            'post' => $posts[$post]
//        ]);

        return view('post',[
            'post' => $post
        ]);
    }
}
